import React, { Component } from 'react';

class GoogleMap extends Component {
  componentDidMount() {
    new google.maps.Map(this.refs.map, {
      zoom: 12,
      center: {
        lat: this.props.lat,
        lng: this.props.lon
      }
    });
  }

  render() {
    return <div ref="map" />;
  }
}

export default GoogleMap;
// import React from 'react';
// import { withGoogleMap, GoogleMap } from 'react-google-maps';

// const GoogleMapExample = withGoogleMap(props =>
//   <GoogleMap
//     defaultCenter={{ lat: props.lat, lng: props.lon }}
//     defaultZoom={12}
//   >
//   </GoogleMap>);

// export default () => {
//   return (
//     <div>
//       <GoogleMapExample
//         containerElement={<div style={{ height: `100px`, width: `100px` }} />}
//         mapElement={<div style={{ height: `100%` }} />}
//       />
//     </div>
//   );
// }

// class Map extends Component {

//   render() {
//     const GoogleMapExample = withGoogleMap(props =>
//       <GoogleMap
//         defaultCenter={{ lat: this.props.lat, lng: this.props.lon }}
//         defaultZoom={12}
//       >
//       </GoogleMap>
//     );

//     return (
//       <div>
//         <GoogleMapExample
//           containerElement={<div style={{ height: `100px`, width: `100px` }} />}
//           mapElement={<div style={{ height: `100%` }} />}
//         />
//       </div>
//     );
//   }
// }

// export default Map;